[![pipeline status](https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad/commits/master)

SurveyPad
=========

One interface to rule them all.

SurveyPad is a tool that manages all CERN survey software. This is based on the features of the old PCTOPO, entirely rewritten in modern C++/Qt.

#### Table of Content ####

[Purpose](#purpose)

[Download](#download)

[Documentation](#documentation)
- [User guide](#user-guide)
- [Doxygen](#doxygen)
- [Other](#other)

[Build instructions](#build-instructions)
- [Requirements](#requirements)
- [Generate project](#generate-project)
- [Build](#build)
- [Tests](#tests)

[Contribute](#contribute)
- [Jira](#jira)
- [Pull requests](#pull-requests)
- [Automatic tests](#automatic-tests)

[License](#license)

Purpose
-------

SurveyPad is the successor of PCTOPO: it is mainly an editor for input files of the different CERN survey software. It is currently in development and not yet available, though you can find all the specifications, planned architecture and documentation in the subfolder [doc](./doc).

Download
--------

You can download the last version of the installer for Windows or Linux from the tag page: https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad/-/tags

Documentation
-------------

### User guide ###

See the documentation page: https://confluence.cern.ch/display/SUS/SurveyPad+User+Guide

### Doxygen ###

The Doxygen documentation is meant for developers only. Follow the [Build instructions](#build-instructions) to set up your projects. Then you can build the `doc` target to create the Doxygen documentation. You will need [Doxygen](https://www.stack.nl/~dimitri/doxygen/download.html#srcbin) and [GraphViz](http://www.graphviz.org/download/#executable-packages) installed and configured.

Once built, you can open the file `build/html/index.html` as an entry point to the documentation.

### Other ###

You can find further documentation in the folder [doc](./doc). In this folder, you'll find:
- the validated specifications of the software
- the UML diagram of the project, and some detailed for different parts. Note that you can open the SVG file in a web browser (avoid Edge as it totally messes up the image), and the `.uxf` files with [Umlet](http://www.umlet.com/).

Build instructions
------------------

Before starting, you can have a look at the documentation about [Getting started with C++](https://confluence.cern.ch/pages/viewpage.action?pageId=22153013) for the CERN survey applications.

### Requirements ###

SurveyPad can be built on Windows or Linux. To do so, you need at least:
- a C++14 compiler
- CMake 3.6+
- NSIS
- TUT
- Qt

For Windows, you can follow the steps in the aforementioned [Getting started with C++](https://confluence.cern.ch/pages/viewpage.action?pageId=22153013) documentation.

For Linux, you have an example of the needed steps in the dockerfiles of the [sus_ci_cppworker](https://gitlab.cern.ch/apc/common/docker-image-susoft-cpp) project (the Docker image used to automatically run the tests on GitLab-CI).
Note that the `devtoolset` trick is only necessary on the CC7 (Cern CentOS 7) as it doesn't provide a C++14 compiler by default.

### Generate project ###

We use CMake to generate projects, thus it is possible to generate projects for MSVC, Eclipse, or simple Unix makefiles. See the [CMake Generators documentation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) page.

To generate the project, just call the `./init.sh` script. This script only pulls submodules necessary for full development, without recursive submodules.

### Build ###

Once generated, you can open your project in the `build/` subfolder. If you use MSVC, you can open the file `build/SurveyPad.sln`.

you can see that CMake has generated several targets, among others:
- `ALL_BUILD` builds all except the doxygen documentation
- `PACKAGE` builds the Windows installer
- `RUN_TESTS` runs all tests provided they were build before
- `ZERO_CHECK` reruns CMake and automatically updates your project
- `doc` builds the Doxygen documentation
- `UnitTests` builds the tests
- `SurveyPad` builds the executable (default project in MSVC)
- `copy_scripts` copies the external scripts from source dir to the binary dir

**Note for Linux:** to run the executable on Linux, you need to use the following command from the `build dir`:
```bash
$ LD_LIBRARY_PATH=. ./SurveyPad
```

### Tests ###

To build the tests, build the target `UnitTests` and run it. We Use TUT to generate unit tests. Note that the tests are automatically performed on Gitlab-CI for each contribution. You can see the results in the [CI page](https://gitlab.cern.ch/apc/susofts/interfaces/SurveyPad/pipelines).

Contribute
----------

To report an issue (bug, or feature request), follow the [Jira](#jira) subsection. For development, please read on.

### Jira ###

Any request, bug or development should have a Jira issue. You can create an issue on the [dedicated Jira board](https://its.cern.ch/jira/browse/SUS). This is mandatory for both the users and the developers.

### Pull requests ###

The most up-to-date stable branch is `master`. The beta branch is `dev`. As stable branches, you **must not** commit directly in them. You need to create a specific branch for your on-going development and commit there. As we use CMake, if you add a file, don't forget to add it in one of the `CMakeFile.txt`!

Once you have finished your work, you should create a Pull Request (PR, or Merge Request) from your branch to `dev`. The description of your PR should include a link to the corresponding task in Jira.

Once your PR has been reviewed by another developer and accepted, it can be merged into `dev`. Note that, for the sake of a nice Git history, your branch needs to be up to date with `dev`. If it is not the case, you will have to rebase, either automatically from GitLab if there are no conflicts, or manually otherwise.

### Automatic tests ###

Automatic tests are performed each time you push a commit. These tests include compilation of `ALL_BUILD` target, and running the `UnitTests` target on Linux, Windows 32 and 64 bits. If the tests don't pass, your PR will not be merged.

For each release, when `master` is updated, GitLab-CI will automatically build the installers.

### Submodules synchronization ###

`update_libraries.sh` was created in order to synchronise lib/SUGL and lib/SurveyLib submodules with the same submodules in plugins/. It doesn't require cloning/pulling any libraries of submodules.

License
-------

The license of this software needs to be determined.

Some images under the LGPL are included in this software. They all come from <https://github.com/KDE/breeze-icons>. Check the file [LGPL.LIB](./LGPL.LIB) for more information about he LGPL license.
The software also uses an icon under CC BY-SA license that comes from <https://nl.wikipedia.org/wiki/Bestand:Gnome-view-fullscreen.svg>.

This software uses OpenSSL. 

Copyright (c) 1998-2019 The OpenSSL Project.  All rights reserved. This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (http://www.openssl.org/)

This product includes cryptographic software written by Eric Young (eay@cryptsoft.com).  This product includes software written by Tim Hudson (tjh@cryptsoft.com).

[OpenSSL license](https://www.openssl.org/source/license-openssl-ssleay.txt)
