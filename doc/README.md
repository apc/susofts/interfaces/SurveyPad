SurveyPad Documentation
=======================

The main documentation files to learn about the purpose and software architecture of the SurveyPad project are listed below in order of creation:

- [GeneralSUInterface_Draft](./GeneralSUInterface_Draft.pdf): which provides an overview of the advantages of having a general graphical interface to manage the different homemade tools used in survey activities and a development strategy for it;
- [User_Guides](./User_Guides.pdf): document containing user guides for all SUSoftware;
- [Specification for SurveyPad](./specs/specifications.pdf): that illustrates the needs and features required of the SurveyPad software and leads to its software architecture;
	- [Short presentation for future users](./specs/Utilisateurs/SurveyPad.pdf)
	- [Architecture plans presentation](./specs/Architecture/SurveyPad%20and%20architecture.pdf)
	- [Specs for Geode plugin](./specs/Geode/SpecificationGEODE.pdf)
- [Student Coffee SurveyPad](./Student%20Coffee/SurveyPad.pdf): a presentation of a prototype of the project;
- [SurveyPad v0.3.00-beta](./v0.3.00-beta/v0.3.00-beta.pdf): a presentation of the third beta release of the project with new features implemented;
- [v0.4.02 state of the app](./v0.4.02%20state%20of%20the%20app/appstate.pdf): state of the application at the end of the first fellow contract.

In the folders, you can find some UXF files. These are some UML diagrams done with the free software [UMLet](https://www.umlet.com/).
