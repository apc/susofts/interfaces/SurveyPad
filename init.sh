#!/bin/bash

INIT_COMMAND="git submodule update --init"

# Initialize SUGL
$INIT_COMMAND lib/SUGL
# Initialize SurveyLib
$INIT_COMMAND lib/SurveyLib
# Initialize SUSoftCMakeCommon
$INIT_COMMAND lib/SUSoftCMakeCommon

# Initialize Plugins
$INIT_COMMAND plugins/calidist-plugin
$INIT_COMMAND plugins/chaba-plugin
$INIT_COMMAND plugins/csgeo-plugin ; # ensure that it gets executed in order
cd plugins/csgeo-plugin/lib/CSGeo ; # ensure that it gets executed in order
if [ $? -eq 0 ]; then
    $INIT_COMMAND ; # ensure that it gets executed in order
    cd ../../../../ ; # ensure that it gets executed in order
else
    echo "changing directory to plugins/csgeo-plugin/lib/CSGeo failed -> initiating failed."
fi
$INIT_COMMAND plugins/geode-plugin
$INIT_COMMAND plugins/lgc-plugin
$INIT_COMMAND plugins/rabot-plugin
$INIT_COMMAND plugins/text-plugin

if [ -d build ]; then
    rm -rf build
else
    echo "unable to find build directory"
fi
mkdir build;
cmake -G "Visual Studio 16 2019" -A x64 -DWITHPLUGINS=ON -S ./source -B ./build;