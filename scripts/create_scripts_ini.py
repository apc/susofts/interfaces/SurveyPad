import re
import subprocess
import os
import configparser

root = os.fsencode(os.getcwd())
files = os.listdir(root)

if not files:
    raise Exception("No files are present in the directory!")

scripts_config = configparser.ConfigParser()

for file in files:
    filename = os.fsdecode(file)
    (fbase, fext) = filename.split('.')
    if fext != "exe":
        print(filename + " script")
        continue
    print("SCRIPT_NAME: " + filename)

    p = subprocess.Popen([filename, "-h"],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         shell=True)
    (output, err) = p.communicate()
    P_STATUS = p.wait()
    if P_STATUS:
        print("There were some errors when trying to get help: " + str(err))
        continue

    external_script = {"path": file.decode("utf-8"), "help": output}

    for line in external_script["help"].decode('utf-8').splitlines():
        line_split = str(line).split(':')

        if not len(line_split) > 1:
            continue

        match str(line_split[0]):
            case "Alias":
                external_script["alias"] = line_split[1]
            case "Default arguments":
                external_script["defaultarguments"] = line_split[1]
            case "Description":
                external_script["description"] = line_split[1]
            case "Documentation":
                external_script["documentation"] = (':').join(line_split[1:])
            case "Expected input extensions":
                external_script["expectedinputextensions"] = ','.join(re.findall(r"[\w\.']+", line_split[1]))
            case "Supported Plugins":
                external_script["supportedplugins"] = ','.join(re.findall(r"[\w\-']+", line_split[1]))

    scripts_config[fbase] = external_script

with open('scripts.ini', 'w') as configfile:
    scripts_config.write(configfile)
