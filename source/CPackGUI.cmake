# Installer:
set(SPAD_LOGO "${CMAKE_CURRENT_LIST_DIR}\\resources\\logo.ico")

# License
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_LIST_DIR}/../LICENSE")

# OVERRIDE
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${CPACK_PACKAGE_NAME} ${CPACK_PACKAGE_VERSION} ${SPAD_VERSION_DESCRIPTION}")
set(CPACK_PACKAGE_ICON ${SPAD_LOGO})
set(CPACK_COMPONENTS_ALL ${CPACK_COMPONENTS_ALL} plugins scripts)
set(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_NAME} ${CPACK_PACKAGE_VERSION} ${SPAD_VERSION_DESCRIPTION}")
set(CPACK_NSIS_HELP_LINK "https://confluence.cern.ch/display/SUS/User+Guides")
# Set installer icon (need .ico !!!)
set(CPACK_NSIS_MUI_ICON ${SPAD_LOGO})
set(CPACK_NSIS_MUI_UNIICON ${SPAD_LOGO})
# Run software after install checkbox
set(CPACK_NSIS_MUI_FINISHPAGE_RUN ${PROJECT_NAME})

SET(CPACK_NSIS_EXTRA_INSTALL_COMMANDS ${CPACK_NSIS_EXTRA_INSTALL_COMMANDS} "
	# Chaba
	WriteRegStr HKCR '.chaba' '' 'ChabaFile'
	WriteRegStr HKCR 'ChabaFile' '' 'Chaba Project File'
	WriteRegStr HKCR 'ChabaFile\\DefaultIcon' '' '$INSTDIR\\SurveyPad.exe, 0'
	WriteRegStr HKCR 'ChabaFile\\shell\\open' '' 'Open Chaba Project File'
	WriteRegStr HKCR 'ChabaFile\\shell\\open\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	WriteRegStr HKCR 'ChabaFile\\shell\\edit' '' 'Edit Chaba Project File'
	WriteRegStr HKCR 'ChabaFile\\shell\\edit\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	# CSGeo
	WriteRegStr HKCR '.csgeo' '' 'CSGeoFile'
	WriteRegStr HKCR 'CSGeoFile' '' 'CSGeo Project File'
	WriteRegStr HKCR 'CSGeoFile\\DefaultIcon' '' '$INSTDIR\\SurveyPad.exe, 0'
	WriteRegStr HKCR 'CSGeoFile\\shell\\open' '' 'Open CSGeo Project File'
	WriteRegStr HKCR 'CSGeoFile\\shell\\open\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	WriteRegStr HKCR 'CSGeoFile\\shell\\edit' '' 'Edit CSGeo Project File'
	WriteRegStr HKCR 'CSGeoFile\\shell\\edit\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	# LGC
	WriteRegStr HKCR '.inp' '' 'LGCFile'
	WriteRegStr HKCR '.lgc' '' 'LGCFile'
	WriteRegStr HKCR '.lgc2' '' 'LGCFile'
	WriteRegStr HKCR 'LGCFile' '' 'LGC Project File'
	WriteRegStr HKCR 'LGCFile\\DefaultIcon' '' '$INSTDIR\\SurveyPad.exe, 0'
	WriteRegStr HKCR 'LGCFile\\shell\\open' '' 'Open LGC Project File'
	WriteRegStr HKCR 'LGCFile\\shell\\open\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	WriteRegStr HKCR 'LGCFile\\shell\\edit' '' 'Edit LGC Project File'
	WriteRegStr HKCR 'LGCFile\\shell\\edit\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	# Rabot
	WriteRegStr HKCR '.rab' '' 'RabotFile'
	WriteRegStr HKCR 'RabotFile' '' 'Rabot Project File'
	WriteRegStr HKCR 'RabotFile\\DefaultIcon' '' '$INSTDIR\\SurveyPad.exe, 0'
	WriteRegStr HKCR 'RabotFile\\shell\\open' '' 'Open Rabot Project File'
	WriteRegStr HKCR 'RabotFile\\shell\\open\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	WriteRegStr HKCR 'RabotFile\\shell\\edit' '' 'Edit Rabot Project File'
	WriteRegStr HKCR 'RabotFile\\shell\\edit\\command' '' '$INSTDIR\\SurveyPad.exe \"%1\"'
	# update
	System::Call 'Shell32:SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'
")

SET(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS ${CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS} "
	DeleteRegKey HKCR '.chaba'
	DeleteRegKey HKCR 'ChabaFile'
	DeleteRegKey HKCR '.csgeo'
	DeleteRegKey HKCR 'CSGeoFile'
	DeleteRegKey HKCR '.inp'
	DeleteRegKey HKCR '.lgc'
	DeleteRegKey HKCR '.lgc2'
	DeleteRegKey HKCR 'LGCFile'
	DeleteRegKey HKCR '.rab'
	DeleteRegKey HKCR 'RabotFile'
")
