/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "ApplicationConfig.hpp"
#include "ui_ApplicationConfig.h"

#include <QDir>
#include <QSettings>

#include <utils/SettingsManager.hpp>

bool ApplicationConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SApplicationConfigObject::operator==(o))
		return false;
	const ApplicationConfigObject &oc = static_cast<const ApplicationConfigObject &>(o);
	return networkconfig == oc.networkconfig && textconfig == oc.textconfig && pointconfig == oc.pointconfig && logsconfig == oc.logsconfig
		&& shortcutsconfig == oc.shortcutsconfig && autoSave == oc.autoSave && projectsInTabs == oc.projectsInTabs && isDockFloatable == oc.isDockFloatable
		&& iconSize == oc.iconSize && freeSpaceMonoplugins == oc.freeSpaceMonoplugins;
}

void ApplicationConfigObject::write() const
{
	SApplicationConfigObject::write();
	// tabs
	networkconfig.write();
	textconfig.write();
	pointconfig.write();
	logsconfig.write();
	shortcutsconfig.write();
	// settings
	QSettings settings;
	settings.setValue("projectsInTabs", projectsInTabs);
	settings.setValue("autoSave", autoSave);
	settings.setValue("isDockFloatable", isDockFloatable);
	settings.setValue("freeSpaceMonoplugins", freeSpaceMonoplugins);
	settings.setValue("iconSize", iconSize);
}

void ApplicationConfigObject::read()
{
	SApplicationConfigObject::read();
	// tabs
	networkconfig.read();
	textconfig.read();
	pointconfig.read();
	logsconfig.read();
	shortcutsconfig.read();
	// settings
	QSettings settings;
	projectsInTabs = settings.value("projectsInTabs", false).toBool();
	autoSave = settings.value("autoSave", false).toBool();
	isDockFloatable = settings.value("isDockFloatable", false).toBool();
	freeSpaceMonoplugins = settings.value("freeSpaceMonoplugins", false).toBool();
	iconSize = settings.value("iconSize", QSize(24, 24)).toSize();
}

ApplicationConfig::ApplicationConfig(QWidget *parent) : SConfigWidget(nullptr, parent), ui(std::make_unique<Ui::ApplicationConfig>())
{
	ui->setupUi(this);
	QString settingFile = SettingsManager::settings().innerSettings("settingsFile");
	ui->lblFile->setText(ui->lblFile->text().arg("file:///" + settingFile).arg(QDir::toNativeSeparators(settingFile)));
}

ApplicationConfig::~ApplicationConfig() = default;

SConfigObject *ApplicationConfig::config() const
{
	auto config = new ApplicationConfigObject();
	// tabs
	auto *tmpNC = dynamic_cast<NetworkConfigObject *>(ui->networkConfigWidget->config());
	if (tmpNC)
		config->networkconfig = *tmpNC;
	auto *tmpTC = dynamic_cast<TextEditorConfigObject *>(ui->textConfigWidget->config());
	if (tmpTC)
		config->textconfig = *tmpTC;
	delete tmpTC;
	auto *tmpPC = dynamic_cast<PointsExportConfigObject *>(ui->pointConfigWidget->config());
	if (tmpPC)
		config->pointconfig = *tmpPC;
	delete tmpPC;
	auto *tmpLC = dynamic_cast<UILogHandlerConfigObject *>(ui->logsConfigWidget->config());
	if (tmpLC)
		config->logsconfig = *tmpLC;
	delete tmpLC;
	auto *tmpSC = dynamic_cast<ShortcutsConfigObject *>(ui->shortcutsConfigWidget->config());
	if (tmpSC)
		config->shortcutsconfig = *tmpSC;
	delete tmpSC;
	// settings
	config->autoUpdate = (SApplicationConfigObject::FileDetectionType)ui->fileAutoUpdate->currentIndex();
	config->projectsInTabs = ui->chkTabs->isChecked();
	config->autoSave = ui->chkAutoSave->isChecked();
	config->isDockFloatable = ui->chkDock->isChecked();
	config->freeSpaceMonoplugins = ui->chkFreeSpaceMonoplugins->isChecked();
	config->iconSize = QSize(ui->spinboxIconSize->value(), ui->spinboxIconSize->value());

	return config;
}

void ApplicationConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const ApplicationConfigObject *>(conf);
	if (!config)
		return;

	// tabs
	ui->networkConfigWidget->setConfig(&config->networkconfig);
	ui->textConfigWidget->setConfig(&config->textconfig);
	ui->pointConfigWidget->setConfig(&config->pointconfig);
	ui->logsConfigWidget->setConfig(&config->logsconfig);
	ui->shortcutsConfigWidget->setConfig(&config->shortcutsconfig);
	// settings
	ui->fileAutoUpdate->setCurrentIndex((int)config->autoUpdate);
	ui->chkTabs->setChecked(config->projectsInTabs);
	ui->chkWindows->setChecked(!config->projectsInTabs);
	ui->chkAutoSave->setChecked(config->autoSave);
	ui->chkDock->setChecked(config->isDockFloatable);
	ui->chkFreeSpaceMonoplugins->setChecked(config->freeSpaceMonoplugins);
	ui->spinboxIconSize->setValue(config->iconSize.width());
}

void ApplicationConfig::reset()
{
	ApplicationConfigObject config;
	config.read();
	setConfig(&config);
}

void ApplicationConfig::restoreDefaults()
{
	ApplicationConfigObject config;
	setConfig(&config);
}
