/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef APPLICATIONCONFIG_HPP
#define APPLICATIONCONFIG_HPP

#include <memory>

#include <editors/text/TextEditorConfig.hpp>
#include <interface/SConfigInterface.hpp>
#include <utils/NetworkConfig.hpp>
#include <utils/PointsExportConfig.hpp>
#include <utils/ShortcutsConfig.hpp>
#include <utils/UILogHandlerConfig.hpp>

namespace Ui
{
class ApplicationConfig;
}

struct ApplicationConfigObject : public SApplicationConfigObject
{
	bool operator==(const SConfigObject &o) const noexcept override;
	virtual void write() const override;
	virtual void read() override;

	NetworkConfigObject networkconfig;
	TextEditorConfigObject textconfig;
	PointsExportConfigObject pointconfig;
	UILogHandlerConfigObject logsconfig;
	ShortcutsConfigObject shortcutsconfig;
	bool projectsInTabs = false;
	bool autoSave = false;
	bool isDockFloatable = false;
	bool freeSpaceMonoplugins = false;
	QSize iconSize = QSize(24, 24);
};

class ApplicationConfig : public SConfigWidget
{
	Q_OBJECT

public:
	ApplicationConfig(QWidget *parent = nullptr);
	~ApplicationConfig() override;

	// SConfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	std::unique_ptr<Ui::ApplicationConfig> ui;
};

#endif // APPLICATIONCONFIG_HPP
