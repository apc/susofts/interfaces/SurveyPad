/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <algorithm>

#include <QApplication>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QFileInfo>
#include <QMdiArea>
#include <QMessageBox>
#include <QMimeData>
#include <QSettings>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <interface/SPluginInterface.hpp>
#include <interface/SPluginLoader.hpp>
#include <interface/STabInterface.hpp>
#include <utils/FileDialog.hpp>
#include <utils/MdiSubWindow.hpp>
#include <utils/NetworkManager.hpp>
#include <utils/ProjectManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/ShortcutsConfig.hpp>
#include <utils/TreeMdiManager.hpp>

#include "../Version.hpp"
#include "ApplicationConfig.hpp"
#include "NewOpenDialog.hpp"
#include "PluginRunAllDialog.hpp"
#include "PluginRunnerDialog.hpp"
#include "SettingsDialog.hpp"

namespace
{
/** Presents the dialog where one plugin has to be chosen. */
SPluginInterface *selectPlugin(QWidget *parent)
{
	NewOpenDialog diag(parent);
	SPluginInterface *ptr = nullptr;
	QObject::connect(&diag, &NewOpenDialog::pluginSelected, [&ptr](SPluginInterface *plg) -> void { ptr = plg; });
	diag.exec();
	return ptr;
}
} // namespace

MainWindow::MainWindow() : ui(std::make_unique<Ui::MainWindow>())
{
	logDebug() << "\n\tMainWindow: New session";
	// UI
	ui->setupUi(this);
	statusBar()->showMessage(tr("Loading..."));
	setupUi();
	setupConnections();
	_state = saveState();
	// settings
	readSettings();
	auto *config = new ApplicationConfigObject();
	config->read();
	SettingsManager::settings().settings(SettingsManager::settings().innerSettings("appName"), config);
	// plugins
	statusBar()->showMessage(tr("Loading Plugins..."));
	loadPlugins();
	statusBar()->showMessage(tr("Ready"));
	// scripts
	loadScripts();
	// network
	NetworkManager::getNetworkManager(); // initializes asynchronous cookies load
}

MainWindow::~MainWindow() = default;

void MainWindow::about()
{
	QString appName = SettingsManager::settings().innerSettings("appName");
	QString description = QString(R"""(
<h1>%1 is a seamless interface for Survey Software</h1>

<p>
%1 is a graphical interface that allows to <em>edit</em> input files from all our software, <em>run</em> them through the interface (most of them are in command line)
and <em>see</em> the output files.
</p>

<p>
To do that, it integrates <em>advanced editor</em> options, like a smart text editor with syntaxic color or autocompletion. In the future, it may even present
a 3D view of the measured points.
</p>

<p>
Documentation can be accessed <a href="https://confluence.cern.ch/display/SUS/SurveyPad+User+Guide">here</a>
</p>
<p>
Current version: <b>%2</b> - <em>%3</em>
</p>
)""")
							  .arg(appName)
							  .arg(SettingsManager::settings().innerSettings("appVersion"))
							  .arg(SettingsManager::settings().innerSettings("appDescription"));
	QMessageBox::about(this, tr("About ") + appName, description);
}

void MainWindow::aboutOpenSSL()
{
	QString description = QString(R"""(
<h1>OpenSSL license</h1>

<p>
Copyright (c) 1998-2019 The OpenSSL Project. All rights reserved. This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (http://www.openssl.org/)
<\p>

<p>
This product includes cryptographic software written by Eric Young (eay@cryptsoft.com). This product includes software written by Tim Hudson (tjh@cryptsoft.com).
<\p>

<p>
<a href="https://www.openssl.org/source/license-openssl-ssleay.txt">OpenSSL license</a>
<\p>
)""");

	QMessageBox::about(this, tr("About OpenSSL"), description);
}

void MainWindow::openSettings()
{
	SettingsDialog(new ApplicationConfig(), this).exec();
}

void MainWindow::reportBug()
{
	QString summary = "[SURVEYPAD] - BUG created via link";
	QString description = R"""(
*Issue description*

Short summary.

*Steps to reproduce*

* What to do
* What happens
* What is expected
)""";
	QString url = "https://its.cern.ch/jira/secure/CreateIssueDetails!init.jspa?pid=16924&issuetype=3&components=26810";
	// summary
	url += "&summary=" + summary;
	// description
	url += "&description=" + description;
	// current version ID in JIRA
	url += QString("&versions=%1").arg(QString::fromStdString(getJiraVersion()));
	// current bug assignee in JIRA
	url += QString("&assignee=%2").arg(QString::fromStdString(getJiraBugAssignee()));
	QDesktopServices::openUrl(QUrl(url));
}

void MainWindow::openDocumentation()
{
	QDesktopServices::openUrl(QUrl("https://confluence.cern.ch/display/SUS/SurveyPad+User+Guide"));
}

bool MainWindow::saveProject(SGraphicalWidget *project)
{
	if (!project)
		project = _currentWidget;
	// Checking if project exists and if the parent plugin supports opening and saving files
	if (!project || project->owner()->getExtensions().isEmpty())
		return false;

	auto path = project->getPath().isEmpty() ? getSaveFileName(this, tr("Save project"), "Title-Example", project->owner()->getExtensions()) : project->getPath();
	return ProjectManager::getProjectManager().saveProject(path, project);
}

bool MainWindow::saveProjectAs(SGraphicalWidget *project)
{
	if (!project)
		project = _currentWidget;
	// Checking if project exists and if the parent plugin supports opening and saving files
	if (!project || project->owner()->getExtensions().isEmpty())
		return false;
	auto path = getSaveFileName(
		this, tr("Save project as"), project->getPath().isEmpty() ? "Title-Example" : QFileInfo(project->getPath()).fileName(), project->owner()->getExtensions());
	return ProjectManager::getProjectManager().saveProject(path, project);
}

void MainWindow::saveAll()
{
	for (auto &project : ProjectManager::getProjectManager().getProjects())
		saveProject(project);
	updateTitle();
}

void MainWindow::newProject(SPluginInterface *plugin)
{
	if (!plugin)
		plugin = selectPlugin(this);
	if (!plugin)
		return;
	auto &pm = ProjectManager::getProjectManager();
	// If monoinstance - check if a graphical interface was created before and if yes - show it
	if (plugin->isMonoInstance())
	{
		auto *window = qobject_cast<MdiSubWindow *>(pm.findMdiWindow(plugin));
		if (window)
		{
			pm.getMdiArea()->setActiveSubWindow(window);
			return;
		}
	}
	// Else new
	pm.newProject(plugin);
}

void MainWindow::closeProject()
{
	ProjectManager::getProjectManager().closeProject();
	updateTitle();
}

void MainWindow::openProjects(SPluginInterface *plugin)
{
	auto &pm = ProjectManager::getProjectManager();

	// Select filepath
	QStringList paths = getOpenFileNames(this, tr("Open projects"), "", pm.getFileExtensions(plugin));
	if (paths.isEmpty() || paths.first().isEmpty())
		return;

	// Open the paths
	bool noPlugin = (plugin == nullptr);
	for (const auto &path : paths)
	{
		// Get plugin corresponding to file
		if (noPlugin)
		{
			plugin = pm.findPluginForFile(path);
			if (!plugin)
			{
				logCritical() << tr("No suitable plugin to open the file has been found.");
				continue;
			}
		}
		pm.openProject(path, plugin);
	}
}

void MainWindow::runProject(SGraphicalWidget *project)
{
	if (!project)
		project = _currentWidget;
	PluginRunnerDialog *runw = new PluginRunnerDialog(project, this);
	runw->setAttribute(Qt::WA_DeleteOnClose);
	runw->setAttribute(Qt::WA_ShowWithoutActivating);
	runw->setWindowFlags(runw->windowFlags() | Qt::X11BypassWindowManagerHint | Qt::WindowDoesNotAcceptFocus);
	runw->setFocusPolicy(Qt::NoFocus);
	if (runw->runProject())
	{
		// The window is required only if the project is run
		runw->show();
		// Adapt the geometry
		if (_runnerGeometry.isValid())
			runw->setGeometry(_runnerGeometry);
		connect(runw, &QObject::destroyed, [this, runw](QObject *obj) { _runnerGeometry = qobject_cast<QWidget *>(obj)->geometry(); });
	}
}

void MainWindow::runAllProjects(SPluginInterface *plugin)
{
	PluginRunAllDialog runw(plugin);
	runw.runAllProjects();
	runw.exec(); // The summary window is required in any case
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	auto &pm = ProjectManager::getProjectManager();
	auto windows = pm.getMdiArea()->subWindowList();

	if (!std::all_of(std::begin(windows), std::end(windows), [](QMdiSubWindow *win) -> bool { return ProjectManager::getProjectManager().closeProject(win); }))
		event->ignore();
	else
	{
		hideOptionalWidgets(false);
		writeSettings();
		logDebug() << "MainWindow: End of session" << std::endl;
		event->accept();
	}
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
	if (!event->mimeData()->hasUrls())
	{
		event->ignore();
		return;
	}

	const auto &urlList = event->mimeData()->urls();
	const bool accept = std::any_of(
		std::cbegin(urlList), std::cend(urlList), [](const auto &url) -> bool { return url.isLocalFile() && QFileInfo(url.toLocalFile()).isFile(); });
	if (accept)
		event->acceptProposedAction();
	else
		event->ignore();
}

void MainWindow::dropEvent(QDropEvent *event)
{
	if (!event->mimeData()->hasUrls())
	{
		event->ignore();
		return;
	}

	event->acceptProposedAction();
	auto &pm = ProjectManager::getProjectManager();
	for (const auto &url : event->mimeData()->urls())
	{
		auto *plugin = pm.findPluginForFile(url.toLocalFile());
		if (!plugin)
		{
			logCritical() << tr("No suitable plugin to open the file has been found.");
			return;
		}

		pm.openProject(url.toLocalFile(), plugin);
	}
}

void MainWindow::setupUi()
{
	updateTitle();

	auto &pm = ProjectManager::getProjectManager();
	setCentralWidget(pm.getMdiArea());
	ui->dockMDI->setWidget(pm.getProjectTree());

	ui->menuShow_docks->addAction(ui->dockLog->toggleViewAction());
	ui->menuShow_docks->addAction(ui->dockMDI->toggleViewAction());
	ui->menuShow_docks->addAction(ui->dockHelp->toggleViewAction());
	ui->dockHelp->setFocusPolicy(Qt::NoFocus);
	ui->dockLog->setFocusPolicy(Qt::NoFocus);
	ui->dockMDI->setFocusPolicy(Qt::NoFocus);
	// shortcuts
	ShortcutsConfigObject shortcutSettings = ShortcutsConfig::getShortcutConfig();
	ui->actionSave->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::SAVE));
	ui->actionCopy->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::COPY));
	ui->actionCut->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::CUT));
	ui->actionPaste->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::PASTE));
	ui->actionUndo->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::UNDO));
	ui->actionRedo->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::REDO));
	ui->actionNew->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::NEWPROJECT));
	ui->actionOpen->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::OPENPROJECT));
	ui->actionSaveAs->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::SAVEAS));
	ui->actionSaveAll->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::SAVEALL));
	ui->actionCloseProject->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::CLOSEPROJECT));
	ui->actionRun->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::RUN));
	ui->actionExit->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::EXIT));
	// network manager logged in
	auto loggedInAction = new QAction(QIcon(":/actions/user-offline"), "", this);
	connect(&NetworkManager::getNetworkManager(), &NetworkManager::logInStatusChanged, [this, loggedInAction](bool isLogged) {
		if (isLogged)
			loggedInAction->setIcon(QIcon(":/actions/user-online"));
		else
			loggedInAction->setIcon(QIcon(":/actions/user-offline"));
	});
	ui->menubar->addAction(loggedInAction);
}

void MainWindow::setupConnections()
{
	connect(ui->actionAbout_Qt, &QAction::triggered, qApp, &QApplication::aboutQt);
	// Plugin handling
	connect(&ProjectManager::getProjectManager(), &ProjectManager::hasChanged, [this]() -> void { setWindowModified(ProjectManager::getProjectManager().isModified()); });
	connect(&SettingsManager::settings(), &SettingsManager::settingsChanged, this, &MainWindow::updateUi);
	connect(&SPluginLoader::getPluginLoader(), &SPluginLoader::pluginLoaded, this, &MainWindow::pluginLoaded);
	connect(&SPluginLoader::getPluginLoader(), &SPluginLoader::pluginUnloaded, this, &MainWindow::pluginUnloaded);
	// Edit handling
	connect(ui->actionUndo, &QAction::triggered, [this]() -> void {
		if (_currentWidget)
			_currentWidget->undo();
	});
	connect(ui->actionRedo, &QAction::triggered, [this]() -> void {
		if (_currentWidget)
			_currentWidget->redo();
	});
	connect(ui->actionCopy, &QAction::triggered, [this]() -> void {
		if (_currentWidget)
			_currentWidget->copy();
	});
	connect(ui->actionPaste, &QAction::triggered, [this]() -> void {
		if (_currentWidget)
			_currentWidget->paste();
	});
	connect(ui->actionCut, &QAction::triggered, [this]() -> void {
		if (_currentWidget)
			_currentWidget->cut();
	});
	// Project handling
	connect(ui->actionSave, &QAction::triggered, [this]() -> void {
		saveProject();
		updateTitle();
	});
	connect(ui->actionSaveAs, &QAction::triggered, [this]() -> void {
		saveProjectAs();
		updateTitle();
	});
	connect(ui->actionSaveAll, &QAction::triggered, this, &MainWindow::saveAll);
	connect(ui->actionNew, &QAction::triggered, this, [this]() { newProject(); });
	connect(ui->actionOpen, &QAction::triggered, this, [this]() { openProjects(); });
	connect(ui->actionCloseProject, &QAction::triggered, this, &MainWindow::closeProject);
	connect(ui->actionRun, &QAction::triggered, this, [this]() { runProject(); });
	// project handling
	connect(ProjectManager::getProjectManager().getMdiArea(), &QMdiArea::subWindowActivated, [this](QMdiSubWindow *window) {
		auto &pm = ProjectManager::getProjectManager();
		// mdiArea handling
		projectChanged(window);
		// Highlights the correspondent tree item when a subWindow is selected
		pm.getProjectTree()->highlightTreeItem(window);
		// Update the asterisk as it is lost automatically whenever the application goes out of focus
		updateTitle();
		setWindowModified(pm.isModified());
	});
	// project tree handling
	auto projectTree = ProjectManager::getProjectManager().getProjectTree();
	connect(projectTree, &TreeMdiManager::currentItemChanged,
		[this]() -> void { ProjectManager::getProjectManager().getMdiArea()->resize(ProjectManager::getProjectManager().getMdiArea()->size() - QSize(0, 1)); });
	connect(projectTree, &TreeMdiManager::newProject, [this](SPluginInterface *plugin) { newProject(plugin); });
	connect(projectTree, &TreeMdiManager::openProject, [this](SPluginInterface *plugin) { openProjects(plugin); });
	connect(projectTree, &TreeMdiManager::runAllProjects, this, &MainWindow::runAllProjects);
	connect(projectTree, &TreeMdiManager::closeProject, [this](QMdiSubWindow *win) -> void {
		ProjectManager::getProjectManager().closeProject(win);
		updateTitle();
	});
}

void MainWindow::loadPlugins()
{
	auto &loader = SPluginLoader::getPluginLoader();
	loader.load();
	logInfo() << QString::number(loader.size()) << "plugins have been loaded from '" << loader.pluginDir() << "'";
	if (loader.size() == 0)
		logCritical() << "No plugins loaded!!!";
	else
	{
		ui->actionNew->setEnabled(true);
		ui->actionOpen->setEnabled(true);
	}
	// Reload the tree based on assigned mdiArea
	ProjectManager::getProjectManager().getProjectTree()->updateTree();
}

void MainWindow::loadScripts()
{
	SPluginLoader::getPluginLoader().loadScripts();
}

void MainWindow::readSettings()
{
	logDebug() << "MainWindow: Using settings file" << SettingsManager::settings().innerSettings("settingsFile");
	QSettings settings;
	settings.beginGroup("MainWindow");
	restoreGeometry(settings.value("geometry").toByteArray());
	restoreState(settings.value("windowState").toByteArray());
	_runnerGeometry = settings.value("runnerDialogGeometry").toRect();
	settings.endGroup();
}

void MainWindow::writeSettings()
{
	if (SettingsManager::settings().shouldWriteSettingsOnQuit())
	{
		QSettings settings;
		settings.beginGroup("MainWindow");
		settings.setValue("geometry", saveGeometry());
		settings.setValue("windowState", saveState());
		settings.setValue("runnerDialogGeometry", _runnerGeometry);
		settings.endGroup();
		SettingsManager::settings().writeToConfigFile();
	}
}

void MainWindow::updateUi(const QString &name)
{
	if (name != SettingsManager::settings().innerSettings("appName"))
		return;
	auto config = SettingsManager::settings().settings<ApplicationConfigObject>(name);

	updateDocks(config.isDockFloatable);
	setIconSize(config.iconSize);
	auto mdiArea = ProjectManager::getProjectManager().getMdiArea();
	mdiArea->setViewMode(config.projectsInTabs ? QMdiArea::ViewMode::TabbedView : QMdiArea::ViewMode::SubWindowView);
	if (config.projectsInTabs == QMdiArea::ViewMode::SubWindowView && mdiArea->currentSubWindow())
		mdiArea->currentSubWindow()->showMaximized();

	SettingsManager::settings().settings(TextEditorConfig::configName(), new TextEditorConfigObject(config.textconfig));
	SettingsManager::settings().settings(PointsExportConfig::configName(), new PointsExportConfigObject(config.pointconfig));
	SettingsManager::settings().settings(UILogHandlerConfig::configName(), new UILogHandlerConfigObject(config.logsconfig));
	SettingsManager::settings().settings(ShortcutsConfig::configName(), new ShortcutsConfigObject(config.shortcutsconfig));
	SettingsManager::settings().settings(NetworkConfig::configName(), new NetworkConfigObject(config.networkconfig));

	setWindowModified(ProjectManager::getProjectManager().isModified());
}

void MainWindow::pluginLoaded(const QString &pluginName)
{
	auto *p = SPluginLoader::getPluginLoader().getPlugin(pluginName);
	if (!p)
		return;

	// config for the plugin
	if (p->hasConfigInterface())
	{
		auto tmp = std::unique_ptr<SConfigWidget>(p->configInterface());
		auto *config = tmp->config(); // We don't know the real type of 'config', so we have to generate it from its UI
		config->read();
		SettingsManager::settings().settings(pluginName, config);
	}
}

void MainWindow::pluginUnloaded(const QString &)
{
}

void MainWindow::projectChanged(QMdiSubWindow *window)
{
	if (!window)
		window = ProjectManager::getProjectManager().getMdiArea()->currentSubWindow(); // fix for crazy bug that call this with nullptr window when we change the settings oO
	if (window && window->widget() == _currentWidget)
		return;
	if (ui->actionHideOptionalWidgets->isChecked())
		hideOptionalWidgets(false);
	// first we disconnect the previous project
	for (auto &c : _pluginconnections)
		disconnect(c);
	_pluginconnections.clear();
	_pluginconnections.reserve(8);

	ui->actionCloseProject->setEnabled(window);
	ui->actionPaste->setEnabled(window);
	ui->actionSave->setEnabled(window);
	ui->actionSaveAll->setEnabled(!ProjectManager::getProjectManager().getMdiArea()->subWindowList().isEmpty());
	ui->actionSaveAs->setEnabled(window);
	ui->actionRun->setEnabled(window);

	auto *sub = qobject_cast<MdiSubWindow *>(window);
	if (!sub || !sub->graphicalWidget())
	{
		_currentWidget = nullptr;
		changeToolbar(nullptr);
		changeActions();
		changeStatusWidget(nullptr);
		changeHelpWidget(nullptr);
		return;
	}

	_currentWidget = sub->graphicalWidget();
	updateTitle();
	// Automatic space freeing for monoplugins
	if (SettingsManager::settings().settings<ApplicationConfigObject>(SettingsManager::settings().innerSettings("appName")).freeSpaceMonoplugins)
	{
		bool isMonoInstance = _currentWidget->owner()->isMonoInstance();
		if (!ui->actionHideOptionalWidgets->isChecked() && isMonoInstance) // if monoinstance plugin and hide the docks not checked
			hideOptionalWidgets(isMonoInstance);
	}
	ui->actionRun->setVisible(_currentWidget->owner()->hasLaunchInterface());
	// we change all actions and widgets
	changeToolbar(_currentWidget->toolbar());
	changeActions();
	changeStatusWidget(_currentWidget->statusWidget());
	changeHelpWidget(_currentWidget->helpWidget());
	// we enable all new connection
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::copyAvailable, ui->actionCopy, &QAction::setEnabled));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::cutAvailable, ui->actionCut, &QAction::setEnabled));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::undoAvailable, ui->actionUndo, &QAction::setEnabled));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::redoAvailable, ui->actionRedo, &QAction::setEnabled));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::toolbarChanged, this, &MainWindow::changeToolbar));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::statusWidgetChanged, this, &MainWindow::changeStatusWidget));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::actionsChanged, this, &MainWindow::changeActions));
	_pluginconnections.push_back(connect(_currentWidget, &SGraphicalWidget::helpWidgetChanged, this, &MainWindow::changeHelpWidget));
	logDebug() << "MainWindow: Change to project" << _currentWidget->windowTitle();
}

void MainWindow::changeStatusWidget(QWidget *w)
{
	if (_currentStatus == w)
		return;
	if (_currentStatus)
		statusBar()->removeWidget(_currentStatus);
	_currentStatus = w;
	if (w)
	{
		statusBar()->addPermanentWidget(w);
		w->show();
		// Title Tab changes together its status bar
		updateTitle();
	}
}

void MainWindow::changeHelpWidget(QWidget *w)
{
	if (_currentHelp == w)
		return;
	_currentHelp = w;
	ui->dockHelp->setWidget(w);
	if (w)
		w->show();
}

void MainWindow::changeActions()
{
	auto change = [this](const std::unordered_multimap<SGraphicalWidget::Menus, QAction *> &actions, bool add) -> void {
		for (const auto &[menu, action] : actions)
		{
			switch (menu)
			{
			case SGraphicalWidget::Menus::file:
				add ? ui->menu_File->addAction(action) : ui->menu_File->removeAction(action);
				break;
			case SGraphicalWidget::Menus::edit:
				add ? ui->menu_Edit->addAction(action) : ui->menu_Edit->removeAction(action);
				break;
			case SGraphicalWidget::Menus::view:
				add ? ui->menu_View->addAction(action) : ui->menu_View->removeAction(action);
				break;
			case SGraphicalWidget::Menus::plugin:
				add ? ui->menu_Plugin->addAction(action) : ui->menu_Plugin->removeAction(action);
				break;
			case SGraphicalWidget::Menus::script:
				add ? ui->menu_Script->addAction(action) : ui->menu_Script->removeAction(action);
				break;
			}
		}
	};

	std::unordered_multimap<SGraphicalWidget::Menus, QAction *> actions;
	if (_currentWidget)
		actions = _currentWidget->actions();
	if (_currentActions == actions)
		return;

	change(_currentActions, false);
	_currentActions = std::move(actions);
	change(_currentActions, true);
}

void MainWindow::changeToolbar(QToolBar *w)
{
	if (_currentToolBar == w)
		return;
	if (_currentToolBar)
		removeToolBar(_currentToolBar);
	_currentToolBar = w;
	if (w)
	{
		addToolBar(Qt::TopToolBarArea, w);
		w->show();
	}
}

void MainWindow::updateDocks(bool isFloatable)
{
	QList<QDockWidget *> docks = findChildren<QDockWidget *>();

	for (auto &dock : docks)
	{
		auto flags = dock->features();
		if (isFloatable)
			dock->setFeatures(flags | QDockWidget::DockWidgetFloatable);
		else
		{
			dock->setFeatures(flags & ~QDockWidget::DockWidgetFloatable);
			dock->setFloating(false);
		}
	}
}

void MainWindow::updateTitle()
{
	static const QString winTitlePrefix = "[*] " + SettingsManager::settings().innerSettings("appName") + " - " + SettingsManager::settings().innerSettings("appVersion");

	if (!ProjectManager::getProjectManager().getMdiArea()->subWindowList().isEmpty() && _currentWidget && !_currentWidget->getPath().isEmpty())
		setWindowTitle(winTitlePrefix + " - " + _currentWidget->getPath());
	else
		setWindowTitle(winTitlePrefix);
}

void MainWindow::hideOptionalWidgets(bool hide)
{
	ui->actionHideOptionalWidgets->setChecked(hide);
	hideDocks(hide);
	hideToolbars(hide);
}

void MainWindow::hideDocks(bool hide)
{
	QList<QDockWidget *> docks = findChildren<QDockWidget *>();

	for (auto &dock : docks)
	{
		// If meant to hide and not hidden
		if (hide && !dock->isHidden())
		{
			dock->setHidden(hide);
			_hiddenDocks.insert(dock);
		}
		// If meant to show and was hidden previously
		else if (!hide && _hiddenDocks.find(dock) != _hiddenDocks.end())
		{
			dock->setHidden(hide);
			_hiddenDocks.erase(dock);
		}
	}
}

void MainWindow::hideToolbars(bool hide)
{
	QList<QToolBar *> toolbars = findChildren<QToolBar *>();

	// For now only these toolbars can be toggled, the rest has to be visible all the time
	ui->appToolBar->setHidden(false);
	for (auto &toolbar : QList{ui->fileToolBar, ui->editToolBar})
	{
		// If meant to hide and not hidden
		if (hide && !toolbar->isHidden())
		{
			toolbar->setHidden(hide);
			_hiddenMainwindowToolbars.insert(toolbar);
		}
		// If meant to show and was hidden previously
		else if (!hide && _hiddenMainwindowToolbars.find(toolbar) != _hiddenMainwindowToolbars.end())
		{
			toolbar->setHidden(hide);
			_hiddenMainwindowToolbars.erase(toolbar);
		}
	}
}

void MainWindow::resetView()
{
	ui->actionHideOptionalWidgets->setChecked(false);
	_hiddenDocks.clear();
	_hiddenMainwindowToolbars.clear();
	restoreState(_state);
	_runnerGeometry = QRect();
}
