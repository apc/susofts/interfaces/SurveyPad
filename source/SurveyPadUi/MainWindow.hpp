/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <memory>
#include <set>
#include <unordered_map>
#include <vector>

#include <QMainWindow>

#include <interface/SGraphicalInterface.hpp>

class QMdiSubWindow;
class QToolBar;

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow();
	~MainWindow() override;

public slots:
	/** About the software */
	void about();
	/** About OpenSSL library */
	void aboutOpenSSL();
	/** Settings */
	void openSettings();
	/** Bug report */
	void reportBug();
	/** Open documentation */
	void openDocumentation();

	/** Save project */
	bool saveProject(SGraphicalWidget *project = nullptr);
	/** Save project as */
	bool saveProjectAs(SGraphicalWidget *project = nullptr);
	/** Save all projects */
	void saveAll();
	/** New project */
	void newProject(SPluginInterface *plugin = nullptr);
	/** Close project */
	void closeProject();
	/** Open project */
	void openProjects(SPluginInterface *plugin = nullptr);
	/** Run project */
	void runProject(SGraphicalWidget *project = nullptr);
	/** Run all projects belonging to the specified plugin */
	void runAllProjects(SPluginInterface *plugin);

protected:
	/** We override close event to ask to save everything before we leave */
	virtual void closeEvent(QCloseEvent *event) override;
	// Drag & Drop
	virtual void dragEnterEvent(QDragEnterEvent *event) override;
	virtual void dropEvent(QDropEvent *event) override;

private:
	void setupUi();
	void setupConnections();
	void loadPlugins();
	void loadScripts();
	void readSettings();
	void writeSettings();
	void updateTitle();

private slots:
	// settings
	void updateUi(const QString &name);
	// plugins
	void pluginLoaded(const QString &pluginName);
	void pluginUnloaded(const QString &pluginName);
	// window update
	void projectChanged(QMdiSubWindow *window);
	void changeStatusWidget(QWidget *w);
	void changeHelpWidget(QWidget *w);
	void changeActions();
	void changeToolbar(QToolBar *w);
	void updateDocks(bool isFloatable);
	// view management
	void hideOptionalWidgets(bool hide);
	void hideDocks(bool hide);
	void hideToolbars(bool hide);
	void resetView();

private:
	std::unique_ptr<Ui::MainWindow> ui;
	/** connections of tcurrent widget */
	std::vector<QMetaObject::Connection> _pluginconnections;
	/** current widget, i.e active subwindow */
	SGraphicalWidget *_currentWidget = nullptr;
	/** current status widget */
	QWidget *_currentStatus = nullptr;
	/** current help widget */
	QWidget *_currentHelp = nullptr;
	/** current actions */
	std::unordered_multimap<SGraphicalWidget::Menus, QAction *> _currentActions;
	/** current toolbar */
	QToolBar *_currentToolBar = nullptr;
	/** default window state (dock and toolbars) */
	QByteArray _state;
	/** default PluginRunnerDialog geometry */
	QRect _runnerGeometry;
	/** set containing the docks that were hidden */
	std::set<QDockWidget *> _hiddenDocks;
	/** set containing the optional toolbars that were hidden - used to free space for Monoinstance plugins */
	std::set<QToolBar *> _hiddenMainwindowToolbars;
};

#endif // MAINWINDOW_HPP
