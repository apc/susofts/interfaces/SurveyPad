#include "NewOpenDialog.hpp"

#include <QGridLayout>
#include <QToolButton>

#include <interface/SPluginInterface.hpp>
#include <interface/SPluginLoader.hpp>

NewOpenDialog::NewOpenDialog(QWidget *parent) : QDialog(parent)
{
	setupUi();
	connect(this, &NewOpenDialog::pluginSelected, this, &QDialog::accept);
}

void NewOpenDialog::setupUi()
{
	setWindowTitle("Chose the plugin to launch");
	setMinimumSize({350, 300});
	auto *layout = new QGridLayout(this);
	fillDialog(layout);

	setLayout(layout);
}

void NewOpenDialog::fillDialog(QGridLayout *layout)
{
	int row = 0, col = 0;
	auto plugins = SPluginLoader::getPluginLoader().plugins();

	for (auto &plugin : plugins)
	{
		layout->addWidget(createWidget(plugin), row, col);
		col++;
		if (col == 3)
		{
			row++;
			col = 0;
		}
	}
}

QWidget *NewOpenDialog::createWidget(SPluginInterface *plugin)
{
	auto *button = new QToolButton(this);
	button->setFixedSize({110, 75});
	button->setIcon(plugin->icon());
	button->setText(plugin->name());
	button->setIconSize({50, 50});
	button->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextUnderIcon);
	connect(button, &QAbstractButton::clicked, [this, plugin]() -> void { emit pluginSelected(plugin); });
	return button;
}
