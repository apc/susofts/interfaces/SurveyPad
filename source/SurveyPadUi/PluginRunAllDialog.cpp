/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "PluginRunAllDialog.hpp"
#include "ui_PluginRunAllDialog.h"

#include <QCloseEvent>
#include <QFileInfo>
#include <QTableWidgetItem>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <interface/SLaunchInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <utils/FileDialog.hpp>
#include <utils/MdiSubWindow.hpp>
#include <utils/ProjectManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "ApplicationConfig.hpp"

class PluginRunAllDialog::_PluginRunAllDialog_pimpl
{
public:
	std::unique_ptr<Ui::PluginRunAllDialog> ui;
	bool wasCancelled = false;
	SPluginInterface *plugin = nullptr;
};

PluginRunAllDialog::PluginRunAllDialog(SPluginInterface *plugin, QWidget *parent) : QDialog(parent), _pimpl(std::make_unique<_PluginRunAllDialog_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::PluginRunAllDialog>();
	_pimpl->ui->setupUi(this);
	_pimpl->ui->statusTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	_pimpl->plugin = plugin;
}

PluginRunAllDialog::~PluginRunAllDialog() = default;

void PluginRunAllDialog::runAllProjects()
{
	// Checks
	if (!_pimpl->plugin)
	{
		logCritical() << tr("No plugin provided!");
		return;
	}

	// Prepare the projects
	auto &pm = ProjectManager::getProjectManager();
	std::vector<SGraphicalWidget *> projects = pm.getProjects();
	projects.erase(std::remove_if(projects.begin(), projects.end(), [this](SGraphicalWidget *project) { return project->owner() != _pimpl->plugin; }), projects.end());
	if (projects.empty())
		return;
	// Setup the progress bar
	_pimpl->ui->progressBar->setMaximum((int)projects.size());

	// Run all the projects
	for (auto *project : projects)
	{
		// Check if is not saved or save not successful
		if (!saveBeforeRun(project))
		{
			addRecord(false, QFileInfo(project->getPath()).baseName(), tr("Project was not saved, run unsuccessful"));
			continue;
		}
		// Setting-up
		auto *launcher = _pimpl->plugin->launchInterface(this);
		if (!launcher)
		{
			addRecord(false, QFileInfo(project->getPath()).baseName(), tr("Launcher for the project could not be created"));
			continue;
		}
		connect(this, &PluginRunAllDialog::stopLaunchers, [launcher]() { launcher->stop(); });
		connect(launcher, &SLaunchObject::finished, project, &SGraphicalWidget::runFinished);
		connect(launcher, &SLaunchObject::finished, [this, launcher, project](bool result) -> void {
			addRecord(result, QFileInfo(project->getPath()).baseName(), result ? tr("Run successful") : tr("Error while running: ") + launcher->error());
		});
		// Running
		project->aboutTorun();
		pm.runProject(launcher, project->getPath());
	}
}

void PluginRunAllDialog::addRecord(bool isSuccessful, const QString &name, const QString &status)
{
	auto *itemName = new QTableWidgetItem(name.isEmpty() ? "New Project" : name);
	auto *itemStatus = new QTableWidgetItem(_pimpl->wasCancelled ? "Run was cancelled (launcher status was '" + status + "')" : status);
	itemStatus->setBackground(isSuccessful && !_pimpl->wasCancelled ? getThemeAdaptedColor(QColor(229, 253, 229)) : getThemeAdaptedColor(QColor(255, 222, 222)));
	itemName->setBackground(isSuccessful && !_pimpl->wasCancelled ? getThemeAdaptedColor(QColor(229, 253, 229)) : getThemeAdaptedColor(QColor(255, 222, 222)));

	int id = _pimpl->ui->statusTable->rowCount();
	_pimpl->ui->statusTable->insertRow(id);
	_pimpl->ui->statusTable->setItem(id, 0, itemName);
	_pimpl->ui->statusTable->setItem(id, 1, itemStatus);
	_pimpl->ui->progressBar->setValue(id + 1);
	_pimpl->ui->statusTable->update(); // For some reason without calling it (probably because of the QProcesses running) the table is not updated

	// If finished
	if (id + 1 == _pimpl->ui->progressBar->maximum())
		prepareSummary();
}

void PluginRunAllDialog::closeEvent(QCloseEvent *e)
{
	if (_pimpl->ui->progressBar->value() == _pimpl->ui->progressBar->maximum()) // If finished - just close
		QDialog::closeEvent(e);
	else
		e->ignore();
}

void PluginRunAllDialog::cancelJob()
{
	emit stopLaunchers();
	_pimpl->wasCancelled = true;
}

void PluginRunAllDialog::prepareSummary()
{
	_pimpl->ui->progressBar->hide();
	_pimpl->ui->okBtn->setEnabled(true);
	_pimpl->ui->stopBtn->setEnabled(false);
	setWindowTitle(windowTitle() + " - Completed");
}

bool PluginRunAllDialog::saveBeforeRun(SGraphicalWidget *project)
{
	if (!project->isModified())
		return true;

	auto config = SettingsManager::settings().settings<ApplicationConfigObject>(SettingsManager::settings().innerSettings("appName"));
	if (config.autoSave)
	{
		const QString path = project->getPath();
		return ProjectManager::getProjectManager().saveProject(
			path.isEmpty() ? getSaveFileName(this, tr("Save project"), "Title-Example", project->owner()->getExtensions()) : path, project);
	}
	auto *window = qobject_cast<MdiSubWindow *>(ProjectManager::getProjectManager().findMdiWindow(project));
	if (window)
		return window->askToSave(true);
	return false;
}
