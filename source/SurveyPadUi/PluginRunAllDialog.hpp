/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef PLUGINRUNALLDIALOG_HPP
#define PLUGINRUNALLDIALOG_HPP

#include <memory>

#include <QDialog>

class SGraphicalWidget;
class SPluginInterface;

/**
 * A dialog running multiple projects for the given plugin with the table visualizing the summary of the run.
 *
 * The dialog runs all the projects corresponding to the plugin provided when calling `PluginRunAllDialog::runAllProjects()`. The dialog allows stopping the run and visualizes progress of run in the real-time.
 * When run is completed or stopped the record is added to the table which serves as the summary for the run. 
 */
class PluginRunAllDialog : public QDialog
{
	Q_OBJECT

public:
	PluginRunAllDialog(SPluginInterface *plugin, QWidget *parent = nullptr);
	~PluginRunAllDialog() override;

	/** Runs all projects. */
	void runAllProjects();

signals:
	/** Signal requesting to cancel run of all launchers. */
	void stopLaunchers();

protected:
	void closeEvent(QCloseEvent *e) override;

private slots:
	/** Stops all the running launchers and does not allow new ones to run. */
	void cancelJob();

private:
	/** Adds a record and presents it with proper background color based on success. */
	void addRecord(bool isSuccessful, const QString &name, const QString &status);
	/** @return true is the project was saved or not modified, false otherwise. */
	bool saveBeforeRun(SGraphicalWidget *project);
	/** Adjusts the window for summary. */
	void prepareSummary();

private:
	/** pimpl */
	class _PluginRunAllDialog_pimpl;
	std::unique_ptr<_PluginRunAllDialog_pimpl> _pimpl;
};

#endif // PLUGINRUNALLDIALOG_HPP
