/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "PluginRunnerDialog.hpp"
#include "ui_PluginRunnerDialog.h"

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <sstream>

#include <QCloseEvent>
#include <QTextCursor>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <interface/SLaunchInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <utils/FileDialog.hpp>
#include <utils/MdiSubWindow.hpp>
#include <utils/ProjectManager.hpp>
#include <utils/SettingsManager.hpp>

#include "ApplicationConfig.hpp"

class PluginRunnerDialog::_PluginRunnerDialog_pimpl
{
public:
	std::unique_ptr<Ui::PluginRunnerDialog> ui;
	SLaunchObject *_launcher = nullptr;
	SGraphicalWidget *project = nullptr;
};

PluginRunnerDialog::PluginRunnerDialog(SGraphicalWidget *project, QWidget *parent) : QDialog(parent), _pimpl(std::make_unique<_PluginRunnerDialog_pimpl>())
{
	_pimpl->project = project;

	_pimpl->ui = std::make_unique<Ui::PluginRunnerDialog>();
	_pimpl->ui->setupUi(this);
	auto *plugin = _pimpl->project->owner();
	_pimpl->ui->lblRun->setText("Running " + plugin->name());
	_pimpl->ui->lblIcon->setPixmap(plugin->icon().pixmap(64));
	setWindowTitle(windowTitle().arg(plugin->name()));
	setFocusPolicy(Qt::NoFocus);
}

PluginRunnerDialog::~PluginRunnerDialog() = default;

void PluginRunnerDialog::closeEvent(QCloseEvent *e)
{
	if (!_pimpl->_launcher->isRunning())
		QDialog::closeEvent(e);
	else
		e->ignore();
}

bool PluginRunnerDialog::runProject()
{
	// Checks
	auto *plugin = _pimpl->project->owner();
	_pimpl->_launcher = plugin->launchInterface();
	const QString &launcherPath = _pimpl->_launcher->exepath();
	if (!_pimpl->_launcher)
	{
		logCritical() << tr("Impossible to start the SLaunchInterface of plugin ") << plugin->name();
		return false;
	}
	else if (launcherPath.isEmpty() || (!QFile::exists(launcherPath) && launcherPath != "INTERNAL_COMPUTATIONS"))
	{
		logCritical() << tr("The path to the executable or the file itself does not exist! The currently set path is: ") << _pimpl->_launcher->exepath();
		return false;
	}
	if (!saveBeforeRun())
	{
		logCritical() << tr("Project was not saved, run unsuccessful");
		return false;
	}

	// Setting-up
	connect(_pimpl->_launcher, &SLaunchObject::started, this, &PluginRunnerDialog::jobStarted);
	connect(_pimpl->_launcher, &SLaunchObject::loading, this, &PluginRunnerDialog::jobUpdate);
	connect(_pimpl->_launcher, &SLaunchObject::finished, this, &PluginRunnerDialog::jobFinished);
	connect(_pimpl->_launcher, &SLaunchObject::finished, _pimpl->project, &SGraphicalWidget::runFinished);

	// Running
	_pimpl->project->aboutTorun();
	ProjectManager::getProjectManager().runProject(_pimpl->_launcher, _pimpl->project->getPath());
	return true;
}

void PluginRunnerDialog::jobStarted()
{
	_pimpl->ui->progressBar->setMaximum(100);
	_pimpl->ui->groupBox->setEnabled(true);
	_pimpl->ui->pushButton->setEnabled(true);
}

void PluginRunnerDialog ::jobUpdate(int percentage, QString message)
{
#ifdef _MSC_VER
#	define LOCALTIME(time, timeres) localtime_s((timeres), (time))
#else
#	define LOCALTIME(time, timeres) localtime_r((time), (timeres))
#endif

	_pimpl->ui->progressBar->setValue(percentage);
	// Text to insert in details:
	auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::tm timeres;
	LOCALTIME(&now, &timeres);
	std::stringstream tmp;
	tmp << "<p><b>";
	tmp << std::put_time(&timeres, "%X");
	tmp << "</b>: <em>" << std::max(0, percentage) << "%</em>";
	if (!message.isEmpty())
		tmp << "<br />" << message;
	tmp << "</p><br />";
	_pimpl->ui->textEdit->moveCursor(QTextCursor::MoveOperation::Start);
	_pimpl->ui->textEdit->insertHtml(QString::fromStdString(tmp.str()));
}

void PluginRunnerDialog::jobFinished(bool result)
{
	if (result)
	{
		_pimpl->ui->progressBar->setValue(100);
		accept();
	}
	else
	{
		logCritical() << "Error while running:" << _pimpl->_launcher->error();
		reject();
	}
}

void PluginRunnerDialog::cancelJob()
{
	_pimpl->_launcher->stop();
}

bool PluginRunnerDialog::saveBeforeRun()
{
	if (!_pimpl->project->isModified())
		return true;

	auto config = SettingsManager::settings().settings<ApplicationConfigObject>(SettingsManager::settings().innerSettings("appName"));
	if (config.autoSave)
	{
		const QString path = _pimpl->project->getPath();
		return ProjectManager::getProjectManager().saveProject(
			path.isEmpty() ? getSaveFileName(this, tr("Save project"), "Title-Example", _pimpl->project->owner()->getExtensions()) : path, _pimpl->project);
	}
	auto *window = qobject_cast<MdiSubWindow *>(ProjectManager::getProjectManager().findMdiWindow(_pimpl->project));
	if (window)
		return window->askToSave(true);
	return false;
}
