/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef PLUGINRUNNERDIALOG_HPP
#define PLUGINRUNNERDIALOG_HPP

#include <memory>

#include <QDialog>

namespace Ui
{
class PluginRunnerDialog;
}

class SGraphicalWidget;

/**
 * A dialog showing the progress of currently run survey software.
 *
 * The dialog requires a SGraphicalWidget only, using which it can access the plugin and its launcher. The project will be run when calling `PluginRunnerDialog::runProject()`.
 * If required the project will be saved before run. The dialog displays the progress bar as well as the details of the process, and the button to stop the process.
 */
class PluginRunnerDialog : public QDialog
{
	Q_OBJECT

public:
	PluginRunnerDialog(SGraphicalWidget *project, QWidget *parent = nullptr);
	~PluginRunnerDialog() override;

	/**
	* Runs the project.
	* @return true if a project could start run, false otherwise.
	*/
	bool runProject();

protected:
	void closeEvent(QCloseEvent *e) override;

private slots:
	/** Slot called when SLaunchObject is started. */
	void jobStarted();
	/** Slot called when new message from the process is received, it places the message in the 'details' section of the dialog. */
	void jobUpdate(int percentage, QString message = "");
	/** Slot called when SLaunchObject finished the process. */
	void jobFinished(bool result);
	/** Stops the process. */
	void cancelJob();

private:
	/** @return true if the project was saved or not modified, false otherwise. */
	bool saveBeforeRun();

private:
	/** pimpl */
	class _PluginRunnerDialog_pimpl;
	std::unique_ptr<_PluginRunnerDialog_pimpl> _pimpl;
};

#endif // PLUGINRUNNERDIALOG_HPP
