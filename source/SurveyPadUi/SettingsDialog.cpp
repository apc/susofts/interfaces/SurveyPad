/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "SettingsDialog.hpp"
#include "ui_SettingsDialog.h"

#include <QApplication>
#include <QPushButton>

#include <interface/SConfigInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <interface/SPluginLoader.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

SettingsDialog::SettingsDialog(SConfigWidget *appConfig, QWidget *parent) : QDialog(parent), ui(std::make_unique<Ui::SettingsDialog>())
{
	// UI
	ui->setupUi(this);
	ui->splitter->setSizes({500, 1000});
	setStyleSheet(QString("alternate-background-color: %1;").arg(getThemeAdaptedColor(QColor(200, 200, 200)).name(QColor::HexArgb)));

	// Restore settings buttons
	QPushButton *btnRestoreAll = ui->buttonBox->addButton("Restore all defaults", QDialogButtonBox::ButtonRole::ResetRole);
	QPushButton *btnRestoreTab = ui->buttonBox->addButton("Restore tab defaults", QDialogButtonBox::ButtonRole::ResetRole);
	QPushButton *btnResetTab = ui->buttonBox->addButton("Remove tab changes", QDialogButtonBox::ButtonRole::ResetRole);
	connect(btnRestoreAll, &QPushButton::clicked, this, &SettingsDialog::restoreAllDefaults);
	connect(btnRestoreTab, &QPushButton::clicked, this, &SettingsDialog::restoreTabDefaults);
	connect(btnResetTab, &QPushButton::clicked, this, &SettingsDialog::resetTabSettings);

	// app settings
	const auto appname = SettingsManager::settings().innerSettings("appName");
	_configs[appname.toStdString()].reset(appConfig);
	ui->listWidget->addItem(new QListWidgetItem(QApplication::windowIcon(), appname));
	ui->stackedWidget->addWidget(appConfig);
	appConfig->reset();
	// plugins settings
	for (auto *plugin : SPluginLoader::getPluginLoader().plugins())
	{
		if (plugin->hasConfigInterface())
		{
			const auto [it, b] = _configs.emplace(plugin->name().toStdString(), plugin->configInterface(this));
			ui->listWidget->addItem(new QListWidgetItem(plugin->icon(), plugin->name()));
			ui->stackedWidget->addWidget(it->second.get());
			it->second->reset();
		}
	}
	ui->listWidget->setCurrentRow(0);
}

SettingsDialog::~SettingsDialog() = default;

void SettingsDialog::accept()
{
	for (auto &[name, config] : _configs)
		SettingsManager::settings().settings(QString::fromStdString(name), config->config());
	SettingsManager::settings().writeToConfigFile();

	QDialog::accept();
}

void SettingsDialog::resetTabSettings()
{
	auto *w = qobject_cast<SConfigWidget *>(ui->stackedWidget->currentWidget());
	if (w)
		w->reset();
}

void SettingsDialog::restoreTabDefaults()
{
	auto *w = qobject_cast<SConfigWidget *>(ui->stackedWidget->currentWidget());
	if (w)
		w->restoreDefaults();
}

void SettingsDialog::restoreAllDefaults()
{
	for (auto &[_, config] : _configs)
		config->restoreDefaults();
}
