/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef SETTINGSDIALOG_HPP
#define SETTINGSDIALOG_HPP

#include <memory>
#include <unordered_map>

#include <QDialog>

class SConfigWidget;

namespace Ui
{
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
	Q_OBJECT

public:
	SettingsDialog(SConfigWidget *appConfig, QWidget *parent = nullptr);
	~SettingsDialog() override;

public slots:
	virtual void accept() override;
	void resetTabSettings();
	void restoreTabDefaults();
	void restoreAllDefaults();

private:
	std::unique_ptr<Ui::SettingsDialog> ui;
	std::unordered_map<std::string, std::unique_ptr<SConfigWidget>> _configs;
};

#endif // SETTINGSDIALOG_HPP
