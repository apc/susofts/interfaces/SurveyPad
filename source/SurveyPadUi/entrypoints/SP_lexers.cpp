#include "SP_lexers.hpp"

#include <interface/SPluginInterface.hpp>
#include <io/CooLexerIO.hpp>
#include <io/LogLexer.hpp>

QList<QString> SP_lexers::getNames()
{
	return {CooLexerIO().language(), LogLexer().language()};
}

QList<std::function<QsciLexer *(QWidget *)>> SP_lexers::getLexers()
{
	return {[](QWidget *parent) -> QsciLexer * { return new CooLexerIO(parent); }, [](QWidget *parent) -> QsciLexer * { return new LogLexer(parent); }};
}
