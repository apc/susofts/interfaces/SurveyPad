#include <tut/tut.hpp>

#include <utils/SettingsManager.hpp>

#include "ApplicationConfig.hpp"

namespace
{
inline ApplicationConfigObject getConfig()
{
	ApplicationConfigObject config;

	config.pointconfig.exportFieldsFrame.removeField("rotation");
	config.textconfig.caretLineVisible = false;
	config.projectsInTabs = true;
	config.autoSave = true;

	return config;
}
} // namespace

namespace tut
{
struct applicationconfigdata
{
	applicationconfigdata() { SettingsManager::init("surveypadtestapp", "version"); }
	~applicationconfigdata() { std::remove(SettingsManager::settings().innerSettings("settingsFile").toUtf8().constData()); }
};

typedef test_group<applicationconfigdata> tg;
tg surveypad_applicationconfig_test_group("Test ApplicationConfig class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
/* *********************** */
/* ApplicationConfigObject */
/* *********************** */

template<>
template<>
void testobject::test<1>()
{
	set_test_name("ApplicationConfigObject: Test of default constructor");

	ApplicationConfigObject config;

	ensure_not(config.projectsInTabs);
	ensure_not(config.autoSave);
	ensure(config.textconfig == TextEditorConfigObject());
	ensure(config.pointconfig == PointsExportConfigObject());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ApplicationConfigObject: Test of empty read()");

	ApplicationConfigObject config;
	config.read();

	ensure_not(config.projectsInTabs);
	ensure_not(config.autoSave);
	ensure(config.textconfig == TextEditorConfigObject());
	ensure(config.pointconfig == PointsExportConfigObject());
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ApplicationConfigObject: Test of read() and write()");

	{
		ApplicationConfigObject tmp = getConfig();
		tmp.write();
	}

	ApplicationConfigObject config;
	config.read();
	// Caret Line
	ensure(config == getConfig());
}

/* ********************** */
/*    ApplicationConfig   */
/* ********************** */

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ApplicationConfig: Test of setConfig(), reset() and restoreDefaults()");

	ApplicationConfig widget;
	// initialization
	{
		auto config = getConfig();
		config.write();
		// we change `config` so it is different from what is written (for `reset()`)
		config.autoSave = false;
		widget.setConfig(&config);
	}
	// test of getConfig()
	{
		std::unique_ptr<ApplicationConfigObject> config(dynamic_cast<ApplicationConfigObject *>(widget.config()));
		ensure(config != nullptr);
		auto tmp = getConfig();
		tmp.autoSave = false;
		;
		ensure(*config == tmp);
	}
	// test of reset()
	{
		widget.reset();
		std::unique_ptr<ApplicationConfigObject> config(dynamic_cast<ApplicationConfigObject *>(widget.config()));
		ensure(config != nullptr);
		ensure(*config == getConfig());
	}
	// test of restoreDefaults()
	{
		widget.restoreDefaults();
		std::unique_ptr<ApplicationConfigObject> config(dynamic_cast<ApplicationConfigObject *>(widget.config()));
		ensure(config != nullptr);
		ensure(*config == ApplicationConfigObject());
	}
}
} // namespace tut
