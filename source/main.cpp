#include <filesystem>
#include <string>

#include <QApplication>
#include <QIcon>
#include <QResource>

#include <ConsoleLogHandler.hpp>
#include <FileLogHandler.hpp>
#include <Logger.hpp>
#include <interface/SPluginLoader.hpp>
#include <io/ShareablePointsListIOCSV.hpp>
#include <io/ShareablePointsListIOHTML.hpp>
#include <io/ShareablePointsListIOJson.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/ProjectManager.hpp>
#include <utils/SettingsManager.hpp>

#include "MainWindow.hpp"
#include "Version.hpp"
#include "entrypoints/SP_lexers.hpp"
#ifdef Q_OS_WIN
#	include <windows.h>
#	include <QWebEngineUrlScheme>
#endif

namespace
{
#ifdef Q_OS_WIN
void initWeb()
{
	// Create a custom schema that applies to all profiles
	QWebEngineUrlScheme cellScheme("surveypad");
	cellScheme.setFlags(QWebEngineUrlScheme::SecureScheme | QWebEngineUrlScheme::LocalScheme);
	QWebEngineUrlScheme::registerScheme(cellScheme);
	// Enable Kerberos
	qputenv("QTWEBENGINE_CHROMIUM_FLAGS", "--auth-server-whitelist=*.cern.ch");
	// Set shared contexts
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts, true);
}
#endif

void initSettings()
{
	SettingsManager::init(QString::fromStdString(getAppName()), QString::fromStdString(getVersion()));
	SettingsManager::settings().innerSettings("appDescription", QString::fromStdString(getVersionDescription()));
	SettingsManager::settings().writeToConfigFile();
}

void initLogs()
{
	namespace fs = std::filesystem;

	fs::path logfile = fs::path(SettingsManager::settings().innerSettings("settingsFile").toStdString()).parent_path() / "SurveyPad.log";
	auto c = new ConsoleLogHandler();
	auto f = new FileLogHandler(FileLogHandler::addDate((logfile.string())));
	c->setThreshold(LogMessage::Type::DEBUG);
	f->setThreshold(LogMessage::Type::DEBUG);
	Logger::getLogger().addHandlers(c, f);
	// remove old log files (more than 15 days)
	FileLogHandler::removeOldLogs(logfile.string());
}

void initSerializers()
{
	auto *jsonbin = new ShareablePointsListIOJSon();
	jsonbin->isBinary(true);
	auto *csv = new ShareablePointsListIOCSV(); // for Excel
	csv->changeMimeType("Csv");
	if (!QLocale::system().name().startsWith("en_"))
		csv->setSeparator(';');
	// First it is JSONBIN because it is used to share ShareablePointsList between plugins
	// Then JSON because it is the only type (with JSONBIN) that can hold the full shareablePointsList structure
	// Then is proper CSV because it is the simplest to do
	// Then we have HTML for Excel and LibreOffice
	// finally, there is Excel CSV, which is NOT in UTF-8
	ClipboardManager::getClipboardManager().add(std::pair{jsonbin, true}, // JSONBIN
		std::pair{new ShareablePointsListIOJSon(), true}, // JSON
		std::pair{new ShareablePointsListIOCSV(), true}, // CSV
		std::pair{new ShareablePointsListIOHTML(), true}, // HTML
		std::pair{csv, true} // Excel CSV
	);
}

void initEntryPoints()
{
	SPluginLoader::getPluginLoader().registerEntryPoint("plugin_lexers", {&SP_lexers::staticMetaObject, nullptr});
}

void handleCommandLineArguments(int argc, char **argv, MainWindow &mainWin)
{
	ProjectManager &pm = ProjectManager::getProjectManager();
	// Open the file
#ifdef Q_OS_WIN
	LPWSTR *szArglist = CommandLineToArgvW(GetCommandLineW(), &argc);
	pm.openFromPath(QString::fromWCharArray(szArglist[1]));
	SGraphicalWidget *project = pm.findProject(QString::fromWCharArray(szArglist[1]));
	LocalFree(szArglist);
#else
	pm.openFromPath(QString(argv[1]));
	SGraphicalWidget *project = pm.findProject(argv[1]);
#endif
	if (!project)
		return;

	char **begin = argv;
	char **end = argv + argc;
	// parse the rest of the arguments
	if (std::find_if(begin, end, [](char *val) { return strcmp(val, "-r") == 0; }) != end) // run
		mainWin.runProject(project);
}
} // namespace

int main(int argc, char **argv)
{
#ifdef Q_OS_WIN
	// Web
	initWeb();
#endif
	// Application
	//qputenv("QT_QPA_PLATFORM", "windows:darkmode=2");
	QApplication app(argc, argv);
	app.setStyle("fusion");
	QResource::registerResource(QApplication::applicationDirPath() + "/resources/resourcesSurveyPad.rcc");
	app.setWindowIcon(QIcon(":/icons/surveypad"));
	// Settings
	initSettings();
	// Logs to have a trace if something happens
	initLogs();
	// Serializers
	initSerializers();
	// Entry points
	initEntryPoints();

	// GUI
	MainWindow mainWin;
	// Show the app
	mainWin.show();
	// Parse command line arguments
	if (argc > 0)
		handleCommandLineArguments(argc, argv, mainWin);

	try
	{
		return app.exec();
	}
	catch (const std::exception &e)
	{
		logFatal() << e.what();
		abort();
	}
}
