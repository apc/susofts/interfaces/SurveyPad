#!/bin/bash

# Get the commit hash of SUGL
cd lib/SUGL
COMMIT_HASH_SUGL=$(git rev-parse HEAD)
cd ../../
# Get the commit hash of SurveyLib
cd lib/SurveyLib
COMMIT_HASH_SURVEYLIB=$(git rev-parse HEAD)
cd ../../
# Get the commit hash of SUSoftCMakeCommon
cd lib/SUSoftCMakeCommon
COMMIT_HASH_SUSOFTCMAKECOMMON=$(git rev-parse HEAD)
cd ../../

echo "lib/SUGL hash: $COMMIT_HASH_SUGL"
echo "lib/SurveyLib hash: $COMMIT_HASH_SURVEYLIB"
echo "lib/SUSoftCMakeCommon hash: COMMIT_HASH_SUSOFTCMAKECOMMON"

# Loop through all directories in plugins
for plugin_dir in plugins/*; do
	cd "$plugin_dir"
	pwd
	git update-index --cacheinfo 160000,"$COMMIT_HASH_SUGL",lib/SUGL
	git update-index --cacheinfo 160000,"$COMMIT_HASH_SUSOFTCMAKECOMMON",lib/SUSoftCMakeCommon
	# Special care for SurveyLib inside of CSGeo-plugin
	if [[ "$plugin_dir" == *"csgeo-plugin" ]]; then
		cd lib/CSGeo
		git update-index --cacheinfo 160000,"$COMMIT_HASH_SURVEYLIB",lib/SurveyLib
		cd ../../
	else
		git update-index --cacheinfo 160000,"$COMMIT_HASH_SURVEYLIB",lib/SurveyLib
	fi
	cd ../../
done
