#!/bin/bash

# Update scripts (requires Python >= 3.10)
cd $(dirname $0) # make sure we execute in this directory
cd scripts
python ./create_scripts_ini.py
